# FPFoam - Force Partitioning in OpenFOAM
The force partitioning method allows to decompose different forces acting on an object, and the methodology is implemented in OpenFOAM based on the works by Menon and Mittal [[1]](#1).


## Introduction

Analysing different force components acting on an object provides better insight into the nature of total force and moment acting on it. The work by Quartapelle and Napolitano [[2]](#2) proved to be the base for this methodology. THe methodology is particularly helpful to identify the nature of forces leading to and derived from the motion of the object, and the vortices formed in the domain. This was implemented for a rigid body immersed in incompressible flow, which is now extended as a functionObject for OpenFOAM in the current work.

## Setting up OpenFOAM

The implementation of the code is explained in VimalKumar et al. [[3]](#3), where some variables are introduced as boundary condition. This section explains the setting up of a simulation case in detail.

1. The `0` folder contains the files for velocity (`U`), pressure (`p`), turbulence parameters like `k`, `omega` (&omega) and `nut`. A new file called `PhiI` is added to this folder with following boundary condition. This boundary condition is added to the object in which the force is decomposed. An example file is shown below, but the boundary conditions are to be changed according to the specific simulation needs.

    ```c++
    boundaryField
    {
        INLET
        {
            type            zeroGradient;
        }
        OUTLET
        {
            type            zeroGradient;
        }
        FRONT
        {
            type            empty;
        }
        BACK
        {
            type            empty;
        }
        FAR_FIELD
        {
            type            symmetry;
        }
        CYLINDER
        {
            type            fpPotentialGradient;
            gradientVec     (0 1 0);
        }
    }
    ```
The vector (0 1 0) represents positive y-direction. This has to be changed according to specific domain setup and requriements.

2. The folder constant contains the details of mesh in the polyMesh folder. The turbulence model is to be mentioned in `turbulenceProperties` file, where the fluid property like dynamic viscosity is mentioned in the `transportProperties` file.

3. The system folder contains information about the numerical modelling and schemes used for the simulation. The equation solvers and tolerances are mentioned in the `fvSolution` dictionary. The following change is added to `fvSolution` file to run the simulation.

    ```c++
    "(Pot|PotFinal|PhiI|PhiIFinal)"
    {
        solver          GAMG;
        smoother        DIC;

        tolerance       1e-06;
        relTol          0.01;
    }
    ```
The `fvSchemes` dictionary in the system directory sets the numerical schemes for terms, such as derivatives in equations, that appear in applications being run. The following change is added to `fvSchemes` file to run the simulation.

```c++
    gradSchemes
    {
        grad(Pot)       Gauss linear;
    }

    divSchemes
    {
        div(grad(Pot))  Gauss linear;
    }
```
The library for `fpPimpleFoam` is added to controlDict dictionary as shown below. This file sets the simulation parameters like CFL, time step, libraries for additional paramter calculations like force, force coefficients etc. Apart from the standard format for the simulation setup following parameters are added to the file to run `fpPimpleFoam`.

```c++
    libs		("libfpBoundaryConditions.so");

    functions
    {
        fpforceCoeff
    {
        type        forceCoeffs;
        libs        ("libfpforceCoeff.so");
        ...
        log         yes;
        writeFields yes;
        patches     (CYLINDER);
        boundarypatches (FAR_FIELD);
        fpforceCoeffDir (0 1 0); positive y direction

        // input keywords for directions of force/moment coefficients
        // refer below for options, and relations

        magUInf     100;
        lRef        3.5;
        Aref        2.2;

        CofR        (0 0 0); // Centre of rotation
        dragDir     (1 0 0);
        liftDir     (0 1 0);
    }
    }
```

To run the simulations in parallel, the file `decomposeParDict` helps to decompose the fluid domain. 

## Results

The force coefficients analysed from the simulations are available in `postProcessing` folder, where the files are saved in .dat format.

## References

<a name="1">[1]</a> 
Menon, K. and Mittal, R (2021). 
On the initiation and sustenance of flow-induced vibration of cylinders: insights from force partitioning 
Journal of Fluid Mechanics, 907, A37.

<a name="2">[2]</a> 
L. Quartapelle and M. Napolitano (1983). 
Force and moment in incompressible flows. 
AIAA Journal, 21(6):911–913.